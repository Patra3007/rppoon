﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_Patrik_Juzbasic
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);
    }
}
