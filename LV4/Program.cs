﻿using System;

namespace LV4_Patrik_Juzbasic
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data = new Dataset("prog.txt");
            Analyzer3rdParty analyze = new Analyzer3rdParty();
            Adapter adapter = new Adapter(analyze);
            Console.WriteLine(adapter.CalculateAveragePerRow(data));

            double[] results = adapter.CalculateAveragePerColumn(data);

            foreach (double result in results)
            {
                Console.WriteLine(result);
            }
        }
    }
}
