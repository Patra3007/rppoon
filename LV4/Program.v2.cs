﻿using System;
using System.Collections.Generic;

namespace LV4_Patrik_Juzbasic_3zad
{
    class Program
    {
        static void Main(string[] args)
        {
            List<IRentable> list = new List<IRentable>();
            Video video = new Video("Hacksaw ridge");
            Book book = new Book("Bait");
            list.Add(video);
            list.Add(book);
            RentingConsolePrinter rent = new RentingConsolePrinter();
            rent.DisplayItems(list);
            rent.PrintTotalPrice(list);


            List<IRentable> Lista2 = new List<IRentable>();
            HotItem TheGlassHotel = new HotItem(new Book("The Glass Hotel"));
            HotItem JužniVetar = new HotItem(new Video("Južni Vetar"));
            Lista2.Add(TheGlassHotel);
            Lista2.Add(JužniVetar);
            RentingConsolePrinter rent_2 = new RentingConsolePrinter();
            rent_2.DisplayItems(Lista2);
            rent_2.PrintTotalPrice(Lista2);
        }
    }
}