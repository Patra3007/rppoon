﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV4_Patrik_Juzbasic_3zad
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }

}
