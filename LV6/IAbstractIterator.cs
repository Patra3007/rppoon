﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPONN._6
{
    interface IAbstractIterator
    {
        Note First();
        Note Next();
        Note IsDone { get; }
        Note Current { get; }
    }
}
