﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kockice1
{
    class FileLogger : ILogger
    {
        private string path;
        public FileLogger(string path)
        {
            this.path = path;
        }
        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.path))
            {
                writer.WriteLine(message);
            }
        }
    }

}
