﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kockice1
{
    class DiceRoller
    {
        private List<Die> dice;
        private List<int> resultForEachRoll;
        private Logger logger;

        // zadatak 4
        private FileLogger fileLogger;
        private ConsoleLogger consoleLogger;

        public DiceRoller()
        {
            this.dice = new List<Die>();
            this.resultForEachRoll = new List<int>();
        }
        public void InsertDie(Die die)
        {
            dice.Add(die);
        }
        public void RollAllDices()
        {
            this.resultForEachRoll.Clear();
            foreach (Die die in dice)
            {
                this.resultForEachRoll.Add(die.Roll());
            }
        }
        public IList<int> GetRollingResults()
        {
            return new System.Collections.ObjectModel.ReadOnlyCollection<int>(this.resultForEachRoll);
        }
        public int DiceCount
        {
            get { return dice.Count; }
        }
        public void LogRollingResults()
        {
            foreach (int result in this.resultForEachRoll)
            {
                logger.Log(result.ToString());
            }
        }
        // zadatak 4
        public void ConsoleRollingResults()
        {
            consoleLogger = new ConsoleLogger();
            consoleLogger.Log(this.GetRollingResults().ToString());
        }
        public void FileRollingResults(string path)
        {
            fileLogger = new FileLogger(path);
            fileLogger.Log(this.GetRollingResults().ToString());

        }
    }

}
