﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kockice1
{
    class Die
    {

        private int numberOfSides;
        private RandomGenerator NewGenerator;
        private Random randomGenerator;
        public int SideNumber
        {
            get { return this.numberOfSides; }

        }

        //1. zadatak
        public Die(int numberOfSides)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = new Random();
        }

        // 2. zadatak
        public Die(int numberOfSides, Random randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.randomGenerator = randomGenerator;
        }

        // 3. zadatak
        public Die(int numberOfSides, RandomGenerator randomGenerator)
        {
            this.numberOfSides = numberOfSides;
            this.NewGenerator = randomGenerator;
        }

        public int Roll()
        {
            int rolledNumber = randomGenerator.Next(1, numberOfSides + 1);
            return rolledNumber;
        }

        // 3. zadatak
        public int Roll(int lowerBound, int upperBound)
        {
            return NewGenerator.NextInt(lowerBound, upperBound);
        }
    }

}
