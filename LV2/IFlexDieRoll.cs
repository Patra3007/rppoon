﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kockice1
{
    interface IFlexibleDiceRoller
    {
        void InsertDie(Die die);
        void RemoveAllDice();
        void RollAllDice();

    }

}
