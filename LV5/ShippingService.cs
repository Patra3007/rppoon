﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV5_RPPON
{
    class ShippingService
    {
        private double price_per_kg;

        public ShippingService(double price_per_kg) { this.price_per_kg = price_per_kg; }

        public double Price(double weight)
        {
            return price_per_kg * weight;
        }

        public override string ToString()
        {
            return "Cijena dostave:";
        }
    }
}
