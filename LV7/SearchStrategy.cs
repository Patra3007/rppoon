﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPOON_7
{
    abstract class SearchStrategy
    {
        public abstract double Search(double[] array,double number);
    }

}
